const express = require('express');
require("dotenv").config({ path: "./config.env" });
const bodyParser = require('body-parser')
const mongoose = require('mongoose');

const app = express();
mongoose.set("strictQuery", false);
mongoose.connect(process.env.mongo_url,
    {
        useNewUrlParser: true,
        useUnifiedTopology: true
    })
    .then(() => console.log('Connexion à MongoDB réussie !'))
    .catch(() => console.log('Connexion à MongoDB échouée !'));

app.use((req, res, next) => {
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content, Accept, Content-Type, Authorization, x-auth-token');
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, PATCH, OPTIONS');
    res.setHeader('Access-Control-Expose-Headers', 'x-auth-token');

    next();
});


app.use(bodyParser.json());


app.get('/', (req, res) => {
    res.send('Hello tourism mada');
  });

app.listen(process.env.PORT || 3000);